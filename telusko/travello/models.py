from django.db import models

# Create your models here.
class Igrice(models.Model):
    
    ime = models.CharField(max_length=100)
    slika = models.ImageField(upload_to='pics')
    desk = models.TextField()
    cena = models.IntegerField()
    ponuda = models.BooleanField(default=False)