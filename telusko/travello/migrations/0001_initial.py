# Generated by Django 3.0.8 on 2020-07-17 12:40

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Igrice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ime', models.CharField(max_length=100)),
                ('slika', models.ImageField(upload_to='pics')),
                ('desk', models.TextField()),
                ('ponuda', models.BooleanField(default=False)),
            ],
        ),
    ]
