from django.shortcuts import render
from .models import Igrice
# Create your views here.

def index(request):

    dests = Igrice.objects.all()
    
    return render(request, "index.html", {'dests': dests})