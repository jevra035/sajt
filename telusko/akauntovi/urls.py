from django.urls import path
from . import views

urlpatterns = [
    path("registracija",views.registracija, name="registracija"),
    path("login",views.login, name="login"),
    path("logout",views.logout,name="logout")
]
